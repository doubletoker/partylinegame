#!/usr/local/bin/python3

import asyncio

from deck import Deck

async def holdem(players):
    people = []
    for player in range(players):
        people.append(input("Enter the name for person at seat %d: " % (player + 1)))
    for person in people:
        print(person)
    the_deck = Deck()
    await the_deck.shuffle()
    print(await the_deck.deal(2, players))
    print(await the_deck.deal(3, 1))
    print(await the_deck.deal(1, 1))
    print(await the_deck.deal(1, 1))
