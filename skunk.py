import asyncio
import die

class Player(object):
    rounds = []
    def __init__(self, name):
        self.name = name
        self.position = "Standing"
        self.points = 0
        self.rounds = []

async def skunk():
    players = []
    dice = [die.Die(6), die.Die(6)]
    # get amount of players
    amount = int(input("Enter the numbers of players: "))
    # create each player
    for player in range(amount):
        players.append(Player(input("Enter a name for player %d: " % (player + 1))))
    #start game loop
    while True:
        # start new round
        # stand everyone up
        for player in players:
            player.position = "Standing"
        while any(player.position == "Standing" for player in players):
            outcome = await die.roll(dice)
            print("Rolled a %d and a %d!" % (outcome[0], outcome[1]))
            #check for snake eyes if so erase round points with 0
            if sum(outcome) == 2:
                for player in [player for player in players if player.position == "Standing"]:
                    if len(player.rounds) > 0:
                        player.rounds = [0] * len(player.rounds)
            if any(come == 1 for come in outcome):
                for player in [player for player in players if player.position == "Standing"]:
                    player.points = 0
                    player.position = "Sitting"
                    player.rounds.append(0)
            #ask each player if they want to continue standing
            for player in [player for player in players if player.position == "Standing"]:
                player.points += sum(outcome)
                if (input("Does %s want to continue standing: " % player.name).lower() == "n"):
                    player.position = "Sitting"
                    player.rounds.append(player.points)
                    print("%s scored %d points" % (player.name, player.points))
        for player in players:
            print("%s scored %d points!" % (player.name, player.points))
        if all([len(player.rounds) == 5 for player in players]):
            break
    for player in players:
        print("%s %d" % (player.name, sum(player.rounds)))