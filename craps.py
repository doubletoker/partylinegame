import asyncio
from die import Die

async def craps():
    the_dice = [Die(6), Die(6)]
    the_sum = 0
    for the_die in the_dice:
        the_sum += await the_die.roll()
    if (the_sum == 2 or the_sum == 3 or the_sum == 12):
        print("You Rolled A %d And Crapped Out!" % the_sum)
    elif (the_sum == 7 or the_sum == 11):
        print("You Rolled A %d And Won!" % the_sum)
    else:
        print("You Rolled A %d And Got A Point" % the_sum)
