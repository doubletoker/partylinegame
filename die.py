#!/usr/local/bin/python3
import argparse
import asyncio
import math
import random

class Die(object):
    def __init__(self, sides):
        self.sides = sides
    async def roll(self):
        return int(math.floor(random.random() * self.sides + 1))

async def roll(dice):
    result = []
    for die in dice:
        result.append(await die.roll())
    return result

async def main(sides, dice):
    the_dice = []
    for index in range(int(dice)):
        the_dice.append(Die(int(sides)))
    print(await roll(the_dice))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("sides")
    parser.add_argument("dice")
    args = parser.parse_args()
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(args.sides, args.dice))
    finally:
        event_loop.close()