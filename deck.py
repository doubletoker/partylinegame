#!/usr/local/bin/python3
import argparse
import asyncio
import math
import random

class Deck(object):
    def __init__(self):
        result = []
        for x in range(52):
            result.append(Card(x))
        self.cards = result
    async def shuffle(self):
        current_deck = self.cards
        the_cards = []
        for x in range(51,-1,-1):
            the_cards.append(current_deck.pop(int(random.random() * (x + 1))))
        self.cards = the_cards
        return the_cards
    async def print_cards(self):
        for card in self.cards:
            print(card)
    async def deal(self, cards, players):
        results = {}
        for y in range(players):
            results[str(y)] = []
        for x in range(cards * players):
            results[str(x % players)].append(self.cards.pop())
        return results
class Card(object):
    def __init__(self,value):
        self.suit = int(value / 13)
        self.value = int(value % 13)
    def __repr__(self):
        result = ""
        if self.value == 0:
            result += "A"
        if self.value == 12:
            result += "K"
        if self.value == 11:
            result += "Q"
        if self.value == 10:
            result += "J"
        if self.value == 9:
            result += "T"
        if self.value < 9 and self.value > 0:
            result += str(self.value + 1)
        if self.suit == 0:
            result += "S"
        if self.suit == 1:
            result += "D"
        if self.suit == 2:
            result += "C"
        if self.suit == 3:
            result += "H"
        return result

async def deal(decks, cards, players):
    for deck in decks:
        print(await deck.deal(cards, players))
        
async def texas_holdem(players):
    the_decks = [Deck()]
    await shuffle(the_decks)
    await deal(the_decks, 2, int(players))
    await deal(the_decks, 3, 1)
    await deal(the_decks, 1, 1)
    await deal(the_decks, 1, 1)

async def shuffle(decks):
    for deck in decks:
        await deck.shuffle()

async def peek(decks):
    for deck in decks:
        await deck.print_cards()

async def main(decks, players):
    the_decks = []
    for index in range(int(decks)):
        the_decks.append(Deck())
    #await peek(the_decks)
    await shuffle(the_decks)
    #await peek(the_decks)
    await texas_holdem(int(players))
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("decks")
    parser.add_argument("players")
    args = parser.parse_args()
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(args.decks, args.players))
    finally:
        event_loop.close()